//elemnetos basicos
//var s= 'Hola';
//var n= 42;
//var b= true;
//var u;

//console.log(  typeof(s));
//console.log( typeof(n));
//console.log( typeof(b));
//console.log( typeof(u));


//funciones
//function saludar(){
	//console.log('hola, soy una funcion');

//}
//saludar();

//funcion multiplicar parametros o argumentos

//function tablaMultiplicar(tabla, hasta){
	//for ( i =0; i <= hasta; i++) 
	//console.log(tabla, 'x', i, '=', tabla *i);
	//}
//tablaMultiplicar(1,10);
//tablaMultiplicar(5,10);


//funcion multiplicar con parametros valor default

//function tablaMultiplicar(tabla, hasta = 10){
	//for(i = 0; i <= hasta; i++)
		//console.log(tabla,'x', i,'=', tabla * i);
//}
//tablaMultiplicar(2);
//tablaMultiplicar(2,15);



//devolucion de valores suma

//function sumar(a,b){
	//return(a + b);
	//console.log('ya he realizado la suma');
//}
//var resultado = sumar(5,5);
//console.log(resultado);

//errores

//function errores(){
	//try{
		//funcion_que_no_existe();
	//}catch(ex){
		//console.log("error detectado:" + ex.description);
	//}
	//console.log("la funcion continua despues del error");
//}
//errores();


//ambito de variables
//var a= 1;
//console.log(a);
//function x(){
	//console.log(a);
	//a = 5;
	//console.log(a);
	//console.log(window.a);
//}
//x();
//console.log(a);

//ambito global y local


//console.log('antes: ',p);
//for (let p= 0; p<3; p++)
//	console.log('-',p);
//console.log('despues: ', p);

//console.log('antes; ', p);
//for(var p = 0; p<3; p++)
//	console.log('-',p);
//console.log('despues: ', p);
//function foo(){
//	if(true){
//		let i= 1;
//	}
//	console.log(i);
//}
 //ar texto1 ='hola a todos';
 //var texto2 ="otro mensaje"; //literales
//console.log(texto1);
 //var texto1 = new String('hola a todos');
 //var texto2 = new String("otro mensaje");//objeto
 //console.log(texto2);
//var rep = texto1.repeat(4);
//console.log(rep);

//var star = texto2.padStart(1);
//console.log(star);


//objetosarray

//var array =['a', 'b', 'c',];
//var array2 =[];
//var array3 =['a',4, true];

//var a=new Array(3);
//var b=new [2];
//console.log(a,b);

//objeto array
//var array = ['a', 'b', 'c'];
//array.push('d');
//array.pop();

//array.unshift('z');
//array.shift();

//var array = [1, 2, 3];
//array.push(4, 5, 6);
//array.push([7, 8, 9]);

//var array = [1, 2, 3];
//array = array.concat(4, 5, 6);
//array = array.concat([7, 8, 9]);

//ejercicios

//function f(x,y=2, z=7){  //ejercicio 1
	//return x+y+z;
//}
//console.log(f(5,undefined));  //resultado 14
//ejercicio2

//var animal = 'kitty';
//var resul = (animal == 'kitty')? 'cute': 'still nice';
//console.log(resul);


//ejercicio3 
//function square(n) {
//return n* n
//}
//var animal = 'kitty';
//var result = '';
//if (animal === 'kitty') {
	//result ='cute';
//} else {
	//result = 'still nice';
//}
//square(result);
//console.log(square);

//ejercicio 4

