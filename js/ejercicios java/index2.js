var array = ['a', 'b', 'c'];
array.push('d');
array.pop();

array.unishift('z');
array.shift();

var array = [1,2,3];
array.push(4,5,6);
array.push([7,8,9]);

var array = [1,2,3];
array = array.concat(4,5,6);
array = array.concat([7,8,9]);
